











app.controller('staffAddCtrl', ['$scope', '$rootScope', 'puppysService', '$filter', 'Auth','$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, puppysService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $scope.staffModel = {
            id: '',
            name: '',
            address: '',
            city: '',
            state: '',
            pincode: '',
            landline: '',
            mobile: '',
            email: '',
            isActive: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.userRoleList = ['', 'ROLE_ADMIN', 'ROLE_WORKFORCE'];

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.staff_add_form != 'undefined' && typeof $scope.staff_add_form.$pristine != 'undefined' && !$scope.staff_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.staff_add_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetStaffCreate = function() {

            $scope.staffModel.user = '';

        }

        $scope.createStaff =    function() {

            $scope.isDataSavingProcess = true;
            var createStaffParam = {};

            createStaffParam.id = 0;
            createStaffParam.name = $scope.staffModel.name;
            createStaffParam.address = $scope.staffModel.address;
            createStaffParam.city = $scope.staffModel.city;
            createStaffParam.state = $scope.staffModel.state;
            createStaffParam.pincode = $scope.staffModel.pincode;
            createStaffParam.landline = $scope.staffModel.landline;
            createStaffParam.mobile = $scope.staffModel.mobileno;
            createStaffParam.email = $scope.staffModel.email;
            createStaffParam.isActive = 1;

            puppysService.createStaff(createStaffParam).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.isDataSavingProcess = false;
                    $scope.formReset();
                    $state.go('app.stafflist1');
                }
            });
        };
    }]);




