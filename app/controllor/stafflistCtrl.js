











app.controller('stafflist1Ctrl', ['$scope','$rootScope', 'puppysService', '$filter', 'Auth', '$state', '$timeout',  function($scope, $rootScope, puppysService, $filter, Auth, $state, $timeout) {
    
        $rootScope.getNavigationBlockMsg = null;
        
        $scope.staffModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            sku: '',
            status: '',
            serverList: null,
            isLoadingProgress:false
        };
        
        $scope.pagePerCount = [10, 20, 50, 100];
        $scope.staffModel.limit = $scope.pagePerCount[0];
        
        $scope.searchFilter = {
           
           value1:'',
            value2:'',
            value3:'',
            value4:'',
             value5:'',
             value6:'',
             value7:'',
             value8:'',
             value9:'',
             value10:''
        };
        
        $scope.searchFilterValue = "";
        
        $scope.initTableFilterTimeoutPromise = null;
        
        $scope.initTableFilter = function()
        {
                if($scope.initTableFilterTimeoutPromise != null)
                {
                    $timeout.cancel($scope.initTableFilterTimeoutPromise);
                }
                $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);  
        }
        $scope.minDate = $scope.minDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = "dd-MMMM-yyyy"
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }
        
        
        //    
        $scope.getList =  function(){
      
            var getListParam = {};
            getListParam.id = "";
            
           getListParam.limit = $scope.staffModel.limit;
            
            getListParam.staffId = $scope.searchFilter.value1;
              getListParam.name = $scope.searchFilter.value2;
              getListParam.qualification = $scope.searchFilter.value3;
              
            $scope.staffModel.isLoadingProgress = true;
       
           
            puppysService.getstafflist1(getListParam).then(function (response) {
                var data = response.data;
                $scope.staffModel.list = data.productList1;
                $scope.staffModel.total = data.total;
                
                $scope.staffModel.isLoadingProgress = false;
            });
      
        };
        
        
        $scope.getList();
    }]);








