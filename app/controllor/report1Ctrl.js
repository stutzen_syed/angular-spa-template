





app.controller('report1Ctrl', ['$scope','$rootScope', 'puppysService', '$filter', 'Auth', '$state', '$timeout',  function($scope, $rootScope, puppysService, $filter, Auth, $state, $timeout) {
    
        $rootScope.getNavigationBlockMsg = null;
        $scope.reportModel = {
            htmlData: '',
            isLoadingProgress:false
        };
        $scope.searchFilter = {
           
            value1:'',
            value2:'',
            value3:'',
            value4:''
            
        };
        
        $scope.searchFilterValue = "";
        
        $scope.initTableFilterTimeoutPromise = null;
        
        $scope.initTableFilter = function()
        {
                if($scope.initTableFilterTimeoutPromise != null)
                {
                    $timeout.cancel($scope.initTableFilterTimeoutPromise);
                }
                $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);  
        }
        $scope.minDate = $scope.minDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = "dd-MMMM-yyyy"
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }
        //    
        $scope.getList =  function(){
      
            var getListParam = {};
            getListParam.reportNo = 1;
            getListParam.dayCode = 22;
            
            $scope.reportModel.isLoadingProgress = true;
       
           
            puppysService.getReport1(getListParam).then(function (response) {
                $scope.reportModel.htmlData = response.data;
                $scope.reportModel.isLoadingProgress = false;
            });
      
        };
    
    }]);




