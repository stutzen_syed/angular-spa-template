app.service('$localStorageService', ['$localStorage', function($localStorage) {

        var $localStorage = this;

        this.set = function(key, value) {

            $localStorage[key] =  JSON.stringify(value);

        };

        this.get = function(key) {

            var retValue = null;

            if (typeof $localStorage[key] != 'undefined')
            {
                retValue = JSON.parse($localStorage[key]);
            }

            return retValue;
        };


        return $localStorage
    }]);
