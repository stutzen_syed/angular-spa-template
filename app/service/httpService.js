app.service("httpService", ['$http', function($http) {

        var $httpService = this;

        var stutzenHttpService = {};

        this.get = function(url, data, handleResponse) {
            if (typeof handleResponse == 'undefined') {
                handleResponse = false;
            }
            var configOption = {};
            configOption.handleResponse = handleResponse;

            return $http({
                method: 'GET',
                url: url,
                params: data,
                config: configOption
            }).success(function(data, status, headers, config) {

            }).error(function(data, status, headers, config) {

            });
        }

        this.post = function(url, data, handleResponse) {
            if (typeof handleResponse == 'undefined') {
                handleResponse = false;
            }
            var configOption = {};
            configOption.handleResponse = handleResponse;

            return $http({
                method: 'POST',
                url: url,
                data: angular.toJson(data, 0),
                config: configOption
            }).success(function(data, status, headers, config) {

            }).error(function(data, status, headers, config) {

            });
        }

        this.formPost = function(url, data, handleResponse) {
            if (typeof handleResponse == 'undefined') {
                handleResponse = false;
            }
            var configOption = {};
            configOption.handleResponse = handleResponse;

            return $http({
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'handleResponse': handleResponse
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: data,
                config: configOption

            }).success(function(data, status, headers, config) {

            }).error(function(data, status, headers, config) {

            });

        }
        return $httpService;

    }]);




