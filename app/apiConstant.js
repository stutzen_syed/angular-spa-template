









app.constant('APP_CONST', {
    USER_INFO_SYNC_API: "/puppys-bms/user/userDetails",
    LOGOUT_API_URL: "/puppys-bms/swf/logout",
    BASE_PATH: "/puppys-bms/",
    API: {
        REPORT1: '/puppys-bms/rp/service/htmlcontent1.json',
        REPORT2: '/puppys-bms/rp/service/htmlcontent2.json',
        STAFFLIST: '/puppys-bms/puppys-bms/rp/service/stafflist1.json',
        STAFF_SAVE: "/puppys-bms/puppys-bms/rp/service/staffsave.json"
    }

})