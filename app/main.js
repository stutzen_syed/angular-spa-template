'use strict';

/* Controllers */

angular.module('app')
        .controller('AppCtrl', ['$scope', '$rootScope', '$translate', '$localStorage', '$window', '$location', '$timeout', '$cookies', 'Auth', 'httpService', 'APP_CONST',
            function($scope, $rootScope, $translate, $localStorage, window, $location, $timeout, $cookies, Auth, httpService, APP_CONST) {

                $rootScope.contextRoot = "/puppys-bms";
                window.onbeforeunload = function(event) {

                    var message = null;
                    if ($rootScope.getNavigationBlockMsg != null)
                    {
                        message = $rootScope.getNavigationBlockMsg();
                    }
                    if (message != null && message.length > 0)
                    {
                        return message;
                    }

                }


                $scope.$on('$destroy', function() {
                    delete window.onbeforeunload;
                });

                $rootScope.company = "Stutzen";
                $rootScope.userModel = {
                    userName: '',
                    id: 23
                };

                $rootScope.path = "projects"
                $rootScope.refer = "";
                $rootScope.isLogined = false;
                $rootScope.isUserModelUpdated = false;
                $rootScope.isUserModelUpdatedProcessing = false;
                $rootScope.selectedProjectKey = "";
                $rootScope.cloudImageAccessKey = {};
                $rootScope.getNavigationBlockMsg = null;
                $rootScope.isNavigationBlockerEnable = false;
                $rootScope.navigationBlockerMessage = "";

                $rootScope.enableNavigationBlocker = function(message) {

                    $rootScope.isNavigationBlockerEnable = true;
                    $rootScope.navigationBlockerMessage = message;
                }

                $rootScope.disableNavigationBlocker = function() {

                    $rootScope.isNavigationBlockerEnable = false;
                    $rootScope.navigationBlockerMessage = "";
                }

                $rootScope.autoSearchList = [];
                $rootScope.autoSearchLimit = 5;

                $rootScope.searchMapLocation = "";
                $rootScope.searchKeyword = "";

                $scope.cookieAcception = typeof $cookies.get('cookie-acception') == 'undefined' ? false : true;

                $scope.actionMsgSuccess = "";
                $scope.actionMsgWarning = "";
                $scope.actionMsgError = "";
                $scope.errorMsgAliveTime = 5000;
                $scope.successMsgAliveTime = 5000;
                $scope.warningMsgAliveTime = 5000;
                $scope.autoSearchServerSyncDelayTime = 1000;

                $scope.autoCompleteFilterList = '';
                $scope.mapSearchOptions = {
                    //country: 'uk',
                    //types: '(cities)'
                };
                $scope.searchMapLocationGecodeDetail = null;
                $scope.searchMapLocationDetail = null;
                //$scope.searchMapLocation = "";
                $scope.autoSearchDetail = null;

                $scope.aliveAutoSearch = false;
                $scope.autoSearchList = [];
                $scope.autoSearchTimeoutPromise = null;
                $scope.autoSearchList1 = [{
                        name: 'Global',
                        searchType: 'Company'
                    }, {
                        name: 'Rosy',
                        searchType: 'Staff'
                    }];

                $scope.$on('updateUserInfoEvent', updateUserInfoEventHandler);
                $scope.$on('httpGenericErrorEvent', genericHttpErrorHandler);
                $scope.$on('httpGenericResponseSuccessEvent', genericHttpResponseHandler);
                $scope.$on('updateMapLocationEvent', updateMapLocationEventHandler);

                $scope.clearErrorMessage = function()
                {
                    $scope.actionMsgError = "";
                    $scope.actionMsgSuccess = "";
                    $scope.actionMsgWarning = "";
                }

                $scope.setErrorMessage = function(msg)
                {
                    $scope.clearErrorMessage();
                    $scope.actionMsgError = msg;
                    $timeout(function() {
                        $scope.actionMsgError = '';
                    }, $scope.errorMsgAliveTime);
                };
                $scope.setSuccessMessage = function(msg)
                {
                    $scope.clearErrorMessage();
                    $scope.actionMsgSuccess = msg;
                    $timeout(function() {
                        $scope.actionMsgSuccess = '';
                    }, $scope.successMsgAliveTime);
                };
                $scope.setWarningMessage = function(msg)
                {
                    $scope.clearErrorMessage();
                    $scope.actionMsgWarning = msg;
                    $timeout(function() {
                        $scope.actionMsgWarning = '';
                    }, $scope.warningMsgAliveTime);
                };

                $scope.getActivePage = function()
                {
                    var retVal = "dashboard"
                    var path = $location.url();
                    if (path.search('/dashboard') != -1)
                    {
                        retVal = 'dashboard';
                    }

                    if (path.search('/stocks') != -1)
                    {
                        retVal = 'stock';
                    }
                    else if (path.search('/purchase-receipt') != -1 || path.search('/add-receipt') != -1 || path.search('/edit-receipt') != -1)
                    {
                        retVal = 'purchasereceipt';
                    }
                    else if (path.search('/orderform') != -1 || path.search('/add-orderform') != -1 || path.search('/edit-orderform') != -1)
                    {
                        retVal = 'orderform';
                    }
                    else if (path.search('/deliverynote') != -1 || path.search('/add-deliverynote') != -1 || path.search('/edit-deliverynote') != -1)
                    {
                        retVal = 'deliverynote';
                    }
                    else if (path.search('/requirement') != -1)
                    {
                        retVal = 'requirement';
                    } else if (path.search('/contact') != -1 || path.search('/add-contact') != -1 || path.search('/edit-contact') != -1)
                    {
                        retVal = 'contact';
                    }

                    return retVal;

                }

                $scope.logout = function()
                {

                    if ($rootScope.getNavigationBlockMsg != null && $rootScope.getNavigationBlockMsg() != "")
                    {
                        window.location.reload();
                    }
                    else
                    {
                        httpService.get(APP_CONST.LOGOUT_API_URL, {}, true).then(function(response) {
                            $scope.setSuccessMessage("You have logout successfully.");
                            $timeout(function() {
                                Auth.setUserModel(null, '');
                            }, 100);
                        });
                    }
                }

                function updateUserInfoEventHandler($event, data)
                {
                    if (data.success == true)
                    {
                        $rootScope.isLogined = true;
                        $rootScope.userModel = data.data.user;
                    }
                    else
                    {
                        $rootScope.isLogined = false;
                        $rootScope.userModel = {
                            username: ''
                        };
                    }
                    $rootScope.isUserModelUpdated = true;
                }

                function updateMapLocationEventHandler($event, data)
                {
                    $scope.searchMapLocationDetail = data;
                    $scope.resolveLocationGeocode();

                }

                function genericHttpErrorHandler($event, response)
                {
                    if (response.status == "404")
                    {
                        $scope.setErrorMessage("Request Not Acceptable.");
                    }
                    else if (response.status == "406")
                    {
                        $scope.setErrorMessage("Request Not Acceptable.");
                    }
                    else if (response.status == "500")
                    {
                        $scope.setErrorMessage("Internal Server Error.");
                    }
                    else if (response.status == "503")
                    {
                        $scope.setErrorMessage("Service Unavailable");
                    }
                    else
                    {
                        $scope.setErrorMessage(response.statusText + " (" + response.status + ")");
                    }

                }

                function genericHttpResponseHandler($event, response)
                {
                    if (typeof response.data != 'undefined')
                    {
                        if (typeof response.data.success != 'undefined')
                        {
                            if (response.data.success == true)
                            {
                                $scope.setSuccessMessage(response.data.message);
                            }
                            else
                            {
                                $scope.setErrorMessage(response.data.message);
                            }

                        }


                    }


                }
                ;

                $scope.setCookie = function(key, value) {

                    var cookieOption = {
                        path: "/"
                    };
                    $cookies.put(key, value, cookieOption);

                };

                $scope.cookieAccept = function() {

                    $scope.setCookie('cookie-acception', true);
                    $scope.cookieAcception = typeof $cookies.get('cookie-acception') == 'undefined' ? false : true;

                };


                $scope.search = function()
                {
                    var lat = "";
                    var lng = "";
                    var loc = "";
                    var typeid = "";
                    var keyid = "";

                    var landingUrl = $window.location.protocol + "//" + $window.location.host + "/app/search/#/result?";

                    if ($rootScope.searchMapLocation != "" || $scope.searchMapLocationDetail != null)
                    {
                        loc = $scope.searchMapLocationDetail.formatted_address;
                    }
                    if ($scope.searchMapLocationGecodeDetail != null)
                    {
                        lat = $scope.searchMapLocationGecodeDetail.lat;
                        lng = $scope.searchMapLocationGecodeDetail.lng;
                    }

                    landingUrl += "loc=" + loc + "&lat=" + lat + "&lng=" + lng;
                    if ($scope.autoSearchDetail != null) {
                        landingUrl += "&typeid=" + $scope.autoSearchDetail.typeId + "&keyid=" + $scope.autoSearchDetail.keyId;
                        landingUrl += "&searchkey=" + $scope.autoSearchDetail.keyString + "&searchtype=" + $scope.autoSearchDetail.typeString;
                    }
                    if ($scope.autoSearchDetail == null) {
                        landingUrl += "&typeid=-1" + "&keyid=1";
                        landingUrl += "&searchkey=" + $scope.searchKeyword + "&searchtype=";
                    }

                    landingUrl += "&page=0&perPage=10";
                    $window.location.href = landingUrl;
                }

                $scope.getContainerClass = function()
                {
                    return $rootScope.containerClass;
                }

                $scope.print = function()
                {
                    window.print();
                }

            }])
        .factory('Auth', function($http, $cookies, $location, $window, $rootScope, APP_CONST) {


            var factory = {};

            //factory.USER_INFO_SYNC_API ="/user/userInfo";

            factory.userModel = null;

            factory.isLogined = function()
            {
                if ($rootScope.isUserModelUpdated)
                    return $rootScope.isLogined;

                return false;

            }

            factory.getUserInfo = function() {

                return $http({
                    method: 'GET',
                    url: APP_CONST.USER_INFO_SYNC_API

                }).success(function(data, status, headers, config) {

                    if (typeof data.data != 'undefined')
                        factory.userModel = data;
                    $rootScope.$broadcast('updateUserInfoEvent', data);

                }).error(function(data, status, headers, config) {

                });

            }

            factory.loginAsync = function()
            {
                if (!factory.isLogined())
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host + "/app/login/#/login-customer" + "?refer=" + encodeURIComponent($location.absUrl());
                    $window.location.href = landingUrl;
                }
            }

            factory.setUserModel = function(userData, referUrl)
            {
                var landingUrl = $window.location.protocol + "//" + $window.location.host;
                var previousRole = "";
                if (factory.userModel != null)
                {
                    previousRole = factory.userModel.userType;
                }
                factory.userModel = userData;


                if (referUrl != "")
                {
                    landingUrl = decodeURIComponent(referUrl);
                }
                if (referUrl == "")
                {
                    if (factory.userModel != null)
                    {
                        if (userData.userType == 1 || userData.userType == 2)
                            landingUrl += APP_CONST.BUSINESS_LOGIN_SUCCESS_REDIRECT;

                    }
                    else
                    {
                        //                      if( || previousRole == 1 || previousRole == 2 )
                        //                        {
                        //                            landingUrl +=  APP_CONST.BUSINESS_LOGIN_SUCCESS_REDIRECT;
                        //                        }
                    }

                }

                $window.location.href = landingUrl;
            }

            return factory;

        })
        .factory('StutzenHttpInterceptor', ['$q', '$injector', '$rootScope', '$window', '$location', function($q, $injector, $rootScope, $window, $location) {

                var stutzenHttpInterceptor = {};

                stutzenHttpInterceptor.responseError = function(response) {

                    $rootScope.$broadcast('httpGenericErrorEvent', response);

                    return $q.reject(response);
                }

                stutzenHttpInterceptor.response = function(response) {


                    //Global Redirect for session out
                    var responseData = "" + response.data;
                    var searchKey = 'swf/login_check'
                    if (responseData.indexOf(searchKey) != -1)
                    {
                        $window.location.href = $window.location.protocol + "//" + $window.location.host + $rootScope.contextRoot + "/login";

                    }

                    if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined')
                    {
                        var config = response.config.config;
                        if (config.handleResponse == true)
                        {
                            $rootScope.$broadcast('httpGenericResponseSuccessEvent', response);
                        }
                    }
                    return response;
                }



                return stutzenHttpInterceptor;

            }])
        .factory('ValidationFactory', function() {

            var validationFactory = {};

            validationFactory.emailValidator = function(email) {

                if (!email) {
                    return;
                }
                //var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                // ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"
                var re = /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i;
                return re.test(email);

            }
            validationFactory.phoneNumberValidator = function(phoneNumber) {

                if (!phoneNumber) {
                    return;
                }
                var re = /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/

                if (!re.test(phoneNumber))
                {
                    return "Please enter a valid phone number. For ex 213-456-7890."
                }
                return true;
            }


            validationFactory.passwordValidator = function(password) {

                if (!password) {
                    return;
                }

                if (password.length < 6) {
                    return "Password must be at least " + 6 + " characters long";
                }

                //		if (!password.match(/[A-Z]/)) {
                //			 return "Password must have at least one capital letter";
                //		}
                //
                //		if (!password.match(/[0-9]/)) {
                //			 return "Password must have at least one number";
                //		}

                return true;
            };



            return validationFactory;

        })
        .factory('StutzenHttpService', ['$q', '$http', function($q, $http) {

                var stutzenHttpService = {};

                stutzenHttpService.get = function(url, data, handleResponse, config) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    if (config != null)
                    {
                        angular.extend(configOption, config);
                    }

                    return $http({
                        method: 'GET',
                        url: url,
                        params: data,
                        config: configOption
                    }).success(function(data, status, headers, config) {

                    }).error(function(data, status, headers, config) {

                    });
                }

                stutzenHttpService.post = function(url, data, handleResponse, config) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    if (config != null)
                    {
                        angular.extend(configOption, config);
                    }
                    return $http({
                        method: 'POST',
                        url: url,
                        data: angular.toJson(data, 0),
                        config: configOption
                    }).success(function(data, status, headers, config) {

                    }).error(function(data, status, headers, config) {

                    });
                }

                stutzenHttpService.formPost = function(url, data, handleResponse) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;

                    return $http({
                        method: 'POST',
                        url: url,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'handleResponse': handleResponse
                        },
                        transformRequest: function(obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: data,
                        config: configOption

                    }).success(function(data, status, headers, config) {

                    }).error(function(data, status, headers, config) {

                    });

                }
                stutzenHttpService.mutipleGet = function(apiData) {

                    var promises = [];

                    for (var i = 0; i < apiData.length; i++)
                    {

                        var deffered = $q.defer();

                        $http({
                            url: apiData[i].url,
                            method: 'GET',
                            params: apiData[i].data
                        }).
                                success(function(data) {
                                    deffered.resolve(data);
                                }).
                                error(function(error) {
                                    deffered.reject();
                                });

                        promises.push(deffered.promise);

                    }

                    return $q.all(promises);

                }
                return stutzenHttpService;

            }])


        .directive('elemReady', function($parse) {
            return {
                restrict: 'A',
                link: function($scope, elem, attrs) {
                    elem.ready(function() {
                        $scope.$apply(function() {
                            var func = $parse(attrs.elemReady);
                            func($scope);
                        })
                    })
                }
            }
        })
        .controller('cloudImageLoaderCtrl', function($scope) {
            $scope.showloder = true;
            $scope.showdownload = false;
            $scope.showpreview = false;
            $scope.imagepath = "";

            //$scope.imagepath = "https://storage.googleapis.com/printhouse/zqSfLnn/38000_test.jpg?GoogleAccessId=printhouse@stutzenme.iam.gserviceaccount.com&Expires=1455009759&Signature=qJmN8H0z84DfpPM%2BGAm7cidlXxv7LbsUJSpOo3JEWlFkkTb3R9A5rHwG7MyFld6W2D8SfWCSMhUBqeiaTaE7DXJiqwMPNffYrgtQl7D30XVQefBltm4xJNA%2Bu4rinyiEJcxKjVAufUHTAYnwfJ5Viue%2BoH9UHKxyrAZ1W7TbFPpEw8mpexnuBx2zSBIeT14O1kCPdq9v1UWPCypcZZ%2BJ%2Br7mMOy30XchPIa0IL9mByeTrwiKFdFaYaHdM2gKlEGSIHae6UPi6QjnjjePWOTe1rWX7e1aawLXhH7ogfNCf%2Fwka2427PjBx3XJEq1Pvv9W%2BIpYGJl9JWtO9k5Q0vD2aw%3D%3D";
            $scope.download = function()
            {
                window.open($scope.imagepath);
            }
            $scope.print = function() {
            }

        })
        .directive('cloudImageLoader', ['$http', '$parse', function($http, $parse) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        urlpath: '=',
                        preview: '='
                    },
                    controller: 'cloudImageLoaderCtrl',
                    template: '<div><img alt="loader" src="img/loading.gif" ng-show="showloder" width="20px;"/><div class="download" ng-click="download()"><img alt="download" src="/img/download.png" ng-show="showdownload"></div><div class="preview" ng-click="download()"><img alt="preview" src="/img/preview.png" ng-show="showpreview"></div></div>',
                    //template:"<img ng-controller='cloudImageLoaderCtrl'  ng-src='{{imagepath}}' ng-if='showImage' alt='Image'/>",
                    link: function($scope, elem, attrs) {

                        if (typeof $scope.$root.cloudImageAccessKey.urlpath == 'undefined')
                        {
                            var requestParam = {};
                            requestParam.verb = 'GET';
                            requestParam.objName = $scope.urlpath;
                            requestParam.contentype = '';

                            $http({
                                method: 'GET',
                                url: '/urlSign/url',
                                params: requestParam,
                                config: {
                                    'key': $scope.urlpath
                                }
                            }).success(function(data, status, headers, config) {
                                /*
                                 * Update the singed Url in facory instance
                                 */
                                $scope.$root.cloudImageAccessKey[config.config.key] = data.data.url;
                                $scope.imagepath = $scope.$root.cloudImageAccessKey[config.config.key];
                                $scope.showloder = false;

                                var fileExt = $scope.urlpath.slice(-3);

                                if (fileExt === "gif" || fileExt === "jpg" || fileExt === "png")
                                {
                                    if ($scope.preview)
                                    {
                                        $scope.showdownload = true;
                                        $scope.showpreview = true;
                                    }
                                    var img = angular.element('<img src="' + $scope.$root.cloudImageAccessKey[config.config.key] + '" />');
                                    elem.append(img);
                                }
                                else
                                {
                                    if ($scope.preview)
                                    {
                                        $scope.showdownload = true;
                                    }
                                    var img = angular.element('<img src="/img/note.png" />');
                                    elem.append(img);
                                }


                            }).error(function(data, status, headers, config) {

                            });

                        }
                        else
                        {
                            $scope.showloder = false;
                            $scope.imagepath = $scope.$root.cloudImageAccessKey[config.config.key];
                            var fileExt = $scope.urlpath.slice(-3);
                            if (fileExt === "gif" || fileExt === "jpg" || fileExt === "png")
                            {
                                if ($scope.preview)
                                {
                                    $scope.showdownload = true;
                                    $scope.showpreview = true;
                                }
                                var img = angular.element('<img src="' + $scope.$root.cloudImageAccessKey[config.config.key] + '" />');
                                elem.append(img);
                            }
                            else
                            {
                                if ($scope.preview)
                                {
                                    $scope.showdownload = true;
                                }
                                var img = angular.element('<img src="/img/note.png" />');
                                elem.append(img);
                            }

                        }

                    }
                }
            }])
        .directive('elemfocusSelect', function($parse) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.on('click', function() {
                        this.select();
                        document.execCommand("copy")
                    });
                }
            };
        })
        .filter('offset', function() {
            return function(input, start) {
                start = parseInt(start, 10);
                return input.slice(start);
            };
        })
        .filter('trustedhtml', function($sce) {
            return $sce.trustAsHtml;
        });


