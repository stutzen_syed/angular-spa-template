'use strict';


angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'pascalprecht.translate',
    'angularValidator',
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.selection',
    'ui.grid.cellNav'

]);
